package pl.pawelLipinski.kafka.example.producer;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import pl.pawelLipinski.kafka.example.User;
import pl.pawelLipinski.kafka.example.configuration.AbstractProducer;
import pl.pawelLipinski.kafka.example.configuration.KafkaConfigurationProperties;
import pl.pawelLipinski.kafka.example.configuration.KafkaTopicConfiguration;
import pl.pawelLipinski.kafka.example.model.ExampleRequest;


@Component
public class ExampleProducer extends AbstractProducer {

    private KafkaTemplate<String, User> kafkaTemplate;

    public ExampleProducer(KafkaConfigurationProperties kafkaConfigurationProperties, KafkaTemplate<String, User> kafkaTemplate) {
        super(kafkaConfigurationProperties);
        this.kafkaTemplate = kafkaTemplate;
    }

    public boolean addMessage(ExampleRequest exampleRequest) {
        ListenableFuture<SendResult<String, User>> future =
                kafkaTemplate.send(KafkaTopicConfiguration.EXAMPLE_TOPIC.getTopicName(), User.newBuilder()
                        .setName(exampleRequest.getName())
                        .setAge(exampleRequest.getAge())
                        .build());

        return waitForAck(future);
    }

}