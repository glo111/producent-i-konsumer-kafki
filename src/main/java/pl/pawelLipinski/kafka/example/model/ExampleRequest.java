package pl.pawelLipinski.kafka.example.model;

import lombok.Data;

@Data
public class ExampleRequest {

    private String name;
    private Integer age;

}
