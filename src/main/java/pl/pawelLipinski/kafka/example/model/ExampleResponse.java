package pl.pawelLipinski.kafka.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ExampleResponse {

    private boolean isDataSavedOnTopic;
}
