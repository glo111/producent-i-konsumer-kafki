package pl.pawelLipinski.kafka.example.configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum KafkaTopicConfiguration {

    EXAMPLE_TOPIC("example_topic");

    private String topicName;

}