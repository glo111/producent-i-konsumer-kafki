package pl.pawelLipinski.kafka.example.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import pl.pawelLipinski.kafka.example.User;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

@Slf4j
public abstract class AbstractProducer {

    protected final KafkaConfigurationProperties kafkaConfigurationProperties;

    public AbstractProducer(KafkaConfigurationProperties kafkaConfigurationProperties) {
        this.kafkaConfigurationProperties = kafkaConfigurationProperties;
    }

    protected boolean waitForAck(ListenableFuture<SendResult<String, User>> ack) {
        try {
            ack.get(kafkaConfigurationProperties.getAckTimeoutMs(), TimeUnit.MILLISECONDS);
            return true;
        } catch (InterruptedException e) {
            log.error("Waiting for ack interrupted", e);
            Thread.currentThread().interrupt();
        } catch (ExecutionException | TimeoutException e) {
            log.error("Exepction while waiting for ack", e);
        } catch (NullPointerException e) {
            return false;
        }
        return false;
    }
}
