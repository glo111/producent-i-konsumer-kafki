package pl.pawelLipinski.kafka.example.configuration;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "pl.example.kafka")
public class KafkaConfigurationProperties {

    //TODO: Dodaj parametry do yml i odpowiednie anotacje do tych zmiennych
    private String bootstrapServer;
    private String schemaRegistryUrl;
    private String acks;
    private String applicationId;
    private String schemaRegistryUser;
    private String schemaRegistryPassword;
    private String jaasConfig;
    private String kafkaUser;
    private String kafkaPassword;
    private String saslMechanism;
    private String securityProtocol;

    private Integer ackTimeoutMs;
    private Integer maxBlock;
    private Integer replicationFactory;

}
