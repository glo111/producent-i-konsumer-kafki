package pl.pawelLipinski.kafka.example.configuration;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.util.StringUtils;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public abstract class AbstractKafkaConfiguration {

    protected final KafkaConfigurationProperties kafkaConfigurationProperties;

    public AbstractKafkaConfiguration(KafkaConfigurationProperties kafkaConfigurationProperties) {
        this.kafkaConfigurationProperties = kafkaConfigurationProperties;
    }

    public Map<String, Object> configureDefaultKafkaProperties() {
        Map<String, Object> configProps = new HashMap<>();
        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConfigurationProperties.getBootstrapServer());
        configProps.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, kafkaConfigurationProperties.getSchemaRegistryUrl());
        configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);
        configProps.put(ProducerConfig.MAX_BLOCK_MS_CONFIG, kafkaConfigurationProperties.getMaxBlock());
        configProps.put(ProducerConfig.CLIENT_ID_CONFIG, kafkaConfigurationProperties.getApplicationId());
        configProps.put(ProducerConfig.ACKS_CONFIG, kafkaConfigurationProperties.getAcks());
        configureSchemaRegistry(configProps);
        configureKafkaAuth(configProps);
        return configProps;
    }

    protected void configureSchemaRegistry(final Map<String, Object> configProps) {
        if (StringUtils.hasText(kafkaConfigurationProperties.getSchemaRegistryUser())) {
            final String userPass = MessageFormat.format(
                    "{0}:{1}",
                    kafkaConfigurationProperties.getSchemaRegistryUser(),
                    kafkaConfigurationProperties.getSchemaRegistryPassword()
            );
            configProps.put(AbstractKafkaAvroSerDeConfig.BASIC_AUTH_CREDENTIALS_SOURCE, "USER_INFO");
            configProps.put(AbstractKafkaAvroSerDeConfig.USER_INFO_CONFIG, userPass);
        } else {
            log.warn("Using no auth for schema registry");
        }
    }

    protected void configureKafkaAuth(final Map<String, Object> configProps) {
        if (StringUtils.hasText(kafkaConfigurationProperties.getKafkaUser())) {
            final String jaasInLineConfig = MessageFormat.format(
                    kafkaConfigurationProperties.getJaasConfig(),
                    kafkaConfigurationProperties.getKafkaUser(),
                    kafkaConfigurationProperties.getKafkaPassword()
            );
            configProps.put("security.protocol", kafkaConfigurationProperties.getSecurityProtocol());
            configProps.put("sasl.mechanism", kafkaConfigurationProperties.getSaslMechanism());
            configProps.put("sasl.jaas.config", jaasInLineConfig);
        } else {
            log.warn("Using no auth for kafka");
        }

    }

}