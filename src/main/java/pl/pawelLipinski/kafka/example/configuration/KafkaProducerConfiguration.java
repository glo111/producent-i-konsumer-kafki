package pl.pawelLipinski.kafka.example.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import pl.pawelLipinski.kafka.example.User;

@Component
public class KafkaProducerConfiguration extends AbstractKafkaConfiguration {

    public KafkaProducerConfiguration(KafkaConfigurationProperties kafkaConfigurationProperties) {
        super(kafkaConfigurationProperties);
    }

    @Bean
    public KafkaTemplate<String, User> kafkaUserTemplate() {
        return new KafkaTemplate<>(new DefaultKafkaProducerFactory<>(configureDefaultKafkaProperties()));
    }

}
