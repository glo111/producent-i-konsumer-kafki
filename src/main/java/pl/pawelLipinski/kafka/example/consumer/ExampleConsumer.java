package pl.pawelLipinski.kafka.example.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import pl.pawelLipinski.kafka.example.User;

import java.io.IOException;

@Service
public class ExampleConsumer {

    private final Logger logger = LoggerFactory.getLogger(ExampleConsumer.class);

    @KafkaListener(topics = "example_topic", groupId = "group_id")
    public void consume(ConsumerRecord<String, User> record) throws IOException {
        logger.info(String.format("#### -> Consumed message -> %s", record.value()));
    }
}
