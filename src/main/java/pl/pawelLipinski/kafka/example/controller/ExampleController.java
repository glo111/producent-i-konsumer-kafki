package pl.pawelLipinski.kafka.example.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.pawelLipinski.kafka.example.model.ExampleRequest;
import pl.pawelLipinski.kafka.example.model.ExampleResponse;
import pl.pawelLipinski.kafka.example.producer.ExampleProducer;

@RestController
@RequestMapping("/api/some-biznes-function")
public class ExampleController {

    private ExampleProducer exampleProducer;

    public ExampleController(ExampleProducer exampleProducer) {
        this.exampleProducer = exampleProducer;
    }

    @PostMapping("/add")
    public ExampleResponse addMessage(@RequestBody ExampleRequest exampleRequest) {
        return new ExampleResponse(exampleProducer.addMessage(exampleRequest));
    }

}
